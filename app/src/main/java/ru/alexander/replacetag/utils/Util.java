package ru.alexander.replacetag.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import ru.alexander.replacetag.BuildConfig;

import java.io.Closeable;

public class Util {

    protected static final String TAG = getTag(Util.class);

    public static String getTag(Class _class) {
        return _class.getName();
    }

    public static Bundle getMetaInfo(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            return applicationInfo.metaData;
        } catch (Exception e) {
            if (BuildConfig.DEBUG)
                Log.e(TAG, "Exception", e);
        }

        return new Bundle();
    }

    public static void close(@Nullable Closeable closeable) {
        if (closeable == null)
            return;

        try {
            closeable.close();
        } catch (Exception e) {
            if (BuildConfig.DEBUG)
                Log.d(TAG, "Exception", e);
        }
    }

}
