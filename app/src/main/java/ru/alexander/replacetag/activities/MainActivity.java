package ru.alexander.replacetag.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.*;
import ru.alexander.replacetag.BuildConfig;
import ru.alexander.replacetag.R;
import ru.alexander.replacetag.utils.Util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends ActionBarActivity {

    protected static final String TAG = Util.getTag(MainActivity.class);

    protected static final Pattern TAG_PATTERN = Pattern.compile("<([^\\s/>]+)");

    protected static final int READ_REQUEST_CODE = 0;
    protected static final int WRITE_REQUEST_CODE = 1;

    protected Toolbar toolbar;

    protected EditText inputFileEditText;
    protected Button selectInputFileButton;
    protected EditText outputFileEditText;
    protected Button selectOutputFileButton;

    protected Spinner tagSpinner;
    protected EditText replacementEditText;
    protected Button replaceTagsButton;

    protected Uri inputFileUri;
    protected Uri outputFileUri;

    protected List<String> tags = new ArrayList<String>();
    protected ArrayAdapter<String> tagSpinnerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        inputFileEditText = (EditText) findViewById(R.id.inputFileEditText);
        selectInputFileButton = (Button) findViewById(R.id.selectInputFileButton);
        outputFileEditText = (EditText) findViewById(R.id.outputFileEditText);
        selectOutputFileButton = (Button) findViewById(R.id.selectOutputFileButton);

        tagSpinner = (Spinner) findViewById(R.id.tagSpinner);
        replacementEditText = (EditText) findViewById(R.id.replacementEditText);
        replaceTagsButton = (Button) findViewById(R.id.replaceTagsButton);

        //

        selectInputFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/xml");
                startActivityForResult(intent, READ_REQUEST_CODE);
            }
        });

        selectOutputFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/xml");
                startActivityForResult(intent, WRITE_REQUEST_CODE);
            }
        });

        tagSpinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, tags);
        tagSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        tagSpinner.setAdapter(tagSpinnerAdapter);
        tagSpinner.setEnabled(false);

        tagSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                updateReplaceTagsButtonState();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                updateReplaceTagsButtonState();
            }
        });

        replacementEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    if (replaceTagsButton.isEnabled())
                        replaceTagsButton.performClick();
                    return true;
                }
                return false;
            }
        });
        replacementEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                updateReplaceTagsButtonState();
            }
        });

        replaceTagsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ReplaceTagsTask().execute(
                        inputFileUri,
                        outputFileUri,
                        tagSpinner.getSelectedItem(),
                        replacementEditText.getText().toString());
            }
        });

        updateReplaceTagsButtonState();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == READ_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                inputFileUri = data.getData();
                if (BuildConfig.DEBUG)
                    Log.i(TAG, "Input file: " + inputFileUri.toString());
                inputFileEditText.setText(inputFileUri.toString());

                new PrepareTask().execute(inputFileUri);
            }
        } else if (requestCode == WRITE_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                outputFileUri = data.getData();
                if (BuildConfig.DEBUG)
                    Log.i(TAG, "Output file: " + outputFileUri.toString());
                outputFileEditText.setText(outputFileUri.toString());
            }
        }

        updateReplaceTagsButtonState();
    }

    protected void updateReplaceTagsButtonState() {
        replaceTagsButton.setEnabled(
                inputFileUri != null
                        && outputFileUri != null
                        && tags.size() > 0
                        && tagSpinner.getSelectedItem() != null
                        && replacementEditText.getText().length() > 0);
    }

    protected class PrepareTask extends AsyncTask<Uri, Void, List<String>> {

        protected Uri uri;

        @Override
        protected List<String> doInBackground(Uri... params) {
            InputStream stream = null;
            InputStreamReader streamReader = null;
            BufferedReader reader = null;

            List<String> tags = new ArrayList<String>();

            try {
                uri = params[0];

                stream = getContentResolver().openInputStream(uri);
                streamReader = new InputStreamReader(stream, "utf-8");
                reader = new BufferedReader(streamReader);

                String line;
                while ((line = reader.readLine()) != null) {
                    Matcher matcher = TAG_PATTERN.matcher(line);
                    while (matcher.find()) {
                        String tag = matcher.group(1);
                        if (!tags.contains(tag))
                            tags.add(tag);
                    }
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "Exception", e);
            } finally {
                Util.close(reader);
                Util.close(streamReader);
                Util.close(stream);
            }

            return tags;
        }

        @Override
        protected void onPostExecute(List<String> strings) {
            if (!uri.equals(inputFileUri))
                return;

            tags.clear();
            tags.addAll(strings);

            tagSpinnerAdapter.notifyDataSetChanged();

            tagSpinner.setSelection(0);
            tagSpinner.setEnabled(true);

            updateReplaceTagsButtonState();
        }

    }

    protected class ReplaceTagsTask extends AsyncTask<Object, Void, Integer> {

        @Override
        protected Integer doInBackground(Object... params) {
            InputStream inputStream = null;
            InputStreamReader streamReader = null;
            BufferedReader reader = null;

            OutputStream outputStream = null;
            OutputStreamWriter streamWriter = null;
            BufferedWriter writer = null;

            int replacementNumber = 0;

            try {
                Uri inputUri = (Uri) params[0];
                Uri outputUri = (Uri) params[1];
                String tag =  (String) params[2];
                String replacement = (String) params[3];

                Pattern pattern = Pattern.compile("(</*)" + Pattern.quote(tag) + "([\\s>])");
                replacement = "$1" + replacement + "$2";

                inputStream = getContentResolver().openInputStream(inputUri);
                streamReader = new InputStreamReader(inputStream, "utf-8");
                reader = new BufferedReader(streamReader);

                outputStream = getContentResolver().openOutputStream(outputUri);
                streamWriter = new OutputStreamWriter(outputStream, "utf-8");
                writer = new BufferedWriter(streamWriter);

                String line;
                while ((line = reader.readLine()) != null) {
                    Matcher matcher = pattern.matcher(line);
                    while (matcher.find())
                        replacementNumber++;

                    writer.write(pattern.matcher(line).replaceAll(replacement));
                    writer.write("\n");
                }
            } catch (Exception e) {
                if (BuildConfig.DEBUG)
                    Log.d(TAG, "Exception", e);
            } finally {
                Util.close(reader);
                Util.close(streamReader);
                Util.close(inputStream);

                Util.close(writer);
                Util.close(streamWriter);
                Util.close(outputStream);
            }

            return replacementNumber;
        }

        @Override
        protected void onPostExecute(Integer replacementNumber) {
            Toast.makeText(MainActivity.this, "Replaced " + replacementNumber + " matches.", Toast.LENGTH_SHORT).show();
        }
    }

}
